(ns main.app
  (:require [reagent.dom :as rdom]))

(defn app []
  [:div "Example Application"])

(defn ^:dev/after-load start []
  (rdom/render [app]
               (.getElementById js/document "app")))

(defn ^:export init []
  (js/console.log "[init]")
  (start))

(defn ^:dev/before-load stop []
  )

